//FREE EXERCICE WITH PATTERNS


(
Pbind(

	a = [240, 270, 290]; //Chords as variables
	b = [443, 444, 480];
	c = [422, 410, 389];
	d = Pwhite(0, 500.0, 50);

	\freq, Prand([a, b, c, d], 99),
    \dur, Pgeom(0.1, 1.001, inf),
	\amp, Prand([0.1, 0.05, 0.08], inf),
	\legato, 0.1,

).trace.play;
)

(
//Play two or more Pbinds together enclosed within a single code block:


Pbind (
	\freq, Pn(Pseries(110, 35, 10)), //Tiene un default de inf?
	\dur, 0.5,
	\legato, Pwhite(0.1, 1)
).play;


Pbind (
	\freq, Pn(Pseries(220, 222, 10)),
	\dur, 1/4,
	\legato, Pwhite (0.1, 1)
).play;

Pbind(
	\freq, Pn(Pseries(330, 333, 10)),
	\dur, 1/6,
	\legato, 0.1
).play;
)